﻿using System;

namespace Linq.Models
{
    public class History
    {
        public int Id { get; set; }
        public DateTime OperationDate { get; set; }

        public enum Type
        {
            refill,
            writeOff
        }

        public Type OperationType { get; set; }
        public decimal Amount { get; set; }
        public int AccountId { get; set; }
        public History(int id, DateTime oDate, Type oType, decimal amount, int accountId)
        {
            this.Id = id;
            this.OperationDate = oDate;
            this.OperationType = oType;
            this.Amount = amount;
            this.AccountId = accountId;
        }
    }
}
