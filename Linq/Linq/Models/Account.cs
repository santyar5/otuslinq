﻿using System;

namespace Linq.Models
{
    public class Account
    {
        public int Id { get; set; }
        public DateTime OpenDate { get; set; }
        public decimal CurrentAmount { get; set; }
        public int UserId { get; set; }
        public Account(int id, DateTime date, decimal amount, int userId)
        {
            this.Id = id;
            this.OpenDate = date;
            this.CurrentAmount = amount;
            this.UserId = userId;
        }
    }
}
