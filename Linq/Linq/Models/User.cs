﻿using System;

namespace Linq.Models
{
    public class User
    {
        public int Id { get; set; }
        public FIO FIO { get; set; }
        public string TelNumb { get; set; }
        public Passport Passp { get; set; }
        public DateTime RegDate { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }

        public User(int id, FIO fio, string telNumb, Passport passp, DateTime regDate, string login, string password)
        {
            this.Id = id;
            this.FIO = fio;
            this.TelNumb = telNumb;
            this.Passp = passp;
            this.RegDate = regDate;
            this.Login = login;
            this.Password = password;
        }
    }
}