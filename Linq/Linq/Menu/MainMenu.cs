﻿using System;
using System.Threading;

namespace Linq.Menu
{
    public static class MainMenu
    {
        public static void OpenMainMenu(string userIn)
        {
            if (userIn.ToLower() == "admin")
            {
                AdminMenu.OpenAdminMenu();
            }
            else
            {
                UserMenu.OpenUserMenu(userIn);
            }
        }
        public static void Close()
        {
            Console.WriteLine("Хорошего дня!");
            Thread.Sleep(5000);
        }
    }
}
