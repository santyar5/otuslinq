﻿using System;
using System.Collections.Generic;
using System.Linq;
using Linq.Models;

namespace Linq
{
    public static class DemoData
    {
        public static List<User> users = new List<User>();
        public static List<Account> accounts = new List<Account>();
        public static List<History> histories = new List<History>();

        public static void CreateData()
        {
            List<string> names = new List<string> { "Иван", "Петр", "Вадим", "Николай", "Валерий" };
            List<string> logins = new List<string> { "Ivan", "Petr", "Vadim", "Nikolay", "Valeriy" };
            List<string> lastNames = new List<string> { "Иванов", "Петров", "Сидоров", "Ромашкин", "Хачатрян" };
            List<string> patronymics = new List<string> { "Викторович", "Константинович", "Олегович", "Павлович", "Михайлович" };

            Random rnd = new Random();
            //добавляем суперпользователя
            users.Add(new User(0, new FIO("Administrator", "", ""), "", new Passport("0000", "000000"),
                new DateTime(1990, 01, 01), "Admin", "SuperPassw0rd"));
            //добавляем киентов
            for (int i = 1; i < 11; i++)
            {
                string name = names.ElementAt(rnd.Next(names.Count));
                users.Add(new User(i,
                    new FIO(name, lastNames.ElementAt(rnd.Next(lastNames.Count)), patronymics.ElementAt(rnd.Next(patronymics.Count))),
                    "8-800-" + rnd.Next(100, 999) + "-" + rnd.Next(10, 99) + "-" + rnd.Next(10, 99),
                    new Passport(rnd.Next(1000, 9999).ToString(), rnd.Next(100000, 999999).ToString()),
                    new DateTime(rnd.Next(1995, 2019), rnd.Next(1, 12), rnd.Next(1, 28)),
                    logins.ElementAt(names.IndexOf(name)) + i,
                    "Passw0rd"));
            }
            //счета клиентов
            int accCounter = 0;
            for (int i = 1; i < users.Count; i++)
            {
                accounts.Add(new Account(accCounter, users.ElementAt(i).RegDate,
                    (decimal)(Math.Round((rnd.Next(1000, 100000) + rnd.NextDouble()) * 100) / 100), i));
                accCounter++;
            }
            //история по счетам
            int histCounter = 0;
            foreach (Account account in accounts)
            {
                histories.Add(new History(histCounter, account.OpenDate,
                    History.Type.refill,
                    account.CurrentAmount + (decimal)(Math.Round((rnd.Next(1000, 100000) + rnd.NextDouble()) * 100) / 100),
                    account.Id));
                histCounter++;

                histories.Add(new History(histCounter, account.OpenDate.AddDays(rnd.Next(1, 100)),
                    History.Type.writeOff,
                    histories.ElementAt(histCounter - 1).Amount - account.CurrentAmount,
                    account.Id));
                histCounter++;
            }
        }
    }
}
