﻿namespace Linq.Models
{
    public class FIO
    {
        public string Name { get; set; }
        public string LastName { get; set; }
        public string Patronymic { get; set; }
        public FIO(string name, string lastName, string patronym)
        {
            this.Name = name;
            this.LastName = lastName;
            this.Patronymic = patronym;
        }

        public override string ToString()
        {
            return $"{LastName} {Name} {Patronymic}";
        }
    }
}
