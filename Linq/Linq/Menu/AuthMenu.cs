﻿using System;
using System.Linq;

namespace Linq.Menu
{
    public static class AuthMenu
    {
        public static void InputLogin()
        {
            Console.WriteLine("Введите логин либо введите 0 для выхода:");
            string userIn = Console.ReadLine();

            if (userIn == "0")
            {
                MainMenu.Close();
            }
            else if (DemoData.users.Where(x => x.Login.ToLower() == userIn.ToLower()).Count() > 0)
            {
                InputPassword(userIn);
            }
            else
            {
                Console.WriteLine("Введен неверный логин.");
                InputLogin();
            }
        }
        public static void InputPassword(string userIn)
        {
            Console.WriteLine("Введите пароль:");
            string password = Console.ReadLine();

            if (DemoData.users.Where(x => x.Login.ToLower() == userIn.ToLower()).Select(x => x.Password).ElementAt(0) == password)
            {
                MainMenu.OpenMainMenu(userIn);
            }
            else
            {
                Console.WriteLine("Введен неверный пароль.");
                InputLogin();
            }
        }
    }
}
