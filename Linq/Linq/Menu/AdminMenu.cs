﻿using System;
using System.Linq;
using Linq.Models;

namespace Linq.Menu
{
    public static class AdminMenu
    {
        public static void OpenAdminMenu()
        {
            Console.WriteLine("Для вывода данных о всех пользователях, нажмите 1.");
            Console.WriteLine("Для вывода данных о всех операциях пополнения пользователей, нажмите 2.");
            Console.WriteLine("Для вывода данных о пользователях, у кого сумма на счете больше указанной, нажмите 3.");
            Console.WriteLine("Для выхода, нажмите 0.");
            string adminInput = Console.ReadLine();

            if (adminInput == "1")
            {
                GetAllUsers();
            }
            else if (adminInput == "2")
            {
                GetRefillOperations();
            }
            else if (adminInput == "3")
            {
                CheckCurrentAmount();
            }
            else if (adminInput == "0")
            {
                AuthMenu.InputLogin();
            }
            else
            {
                Console.WriteLine("Введено неверное значение.");
                OpenAdminMenu();
            }
        }
        public static void GetAllUsers()
        {
            foreach (User user in DemoData.users)
            {
                Console.WriteLine($"ID: {user.Id}; Login: {user.Login}; FIO: {user.FIO.ToString()}");
            }
            OpenAdminMenu();
        }

        public static void GetRefillOperations()
        {
            var operations = from his in DemoData.histories
                             join acc in DemoData.accounts on his.AccountId equals acc.Id
                             join us in DemoData.users on acc.UserId equals us.Id
                             where his.OperationType == History.Type.refill
                             select new { us.FIO, acc.Id, his.OperationDate, his.Amount };
            foreach (var opers in operations)
            {
                Console.WriteLine(opers.FIO.ToString() + " ID счета: " + opers.Id
                    + " Дата операции: " + opers.OperationDate.ToShortDateString() + " Сумма пополнения: " + opers.Amount);
            }
            OpenAdminMenu();
        }

        public static void CheckCurrentAmount()
        {
            Console.WriteLine("Введите целую сумму:");
            string sumInput = Console.ReadLine();
            int parsed = 0;
            if (int.TryParse(sumInput, out parsed))
            {
                var filtered = DemoData.accounts.Join(DemoData.users,
                    a => a.UserId,
                    u => u.Id,
                    (a, u) => new { u.FIO, a.Id, a.CurrentAmount })
                    .Where(x => x.CurrentAmount >= parsed);
                if (filtered.Count() > 0)
                {
                    foreach (var amount in filtered)
                    {
                        Console.WriteLine($"Счет: {amount.Id} Владелец: {amount.FIO.ToString()} Остаток {amount.CurrentAmount}");
                    }
                }
                else
                {
                    Console.WriteLine($"Нет полльзователей с остатком по счету более {parsed} рублей");
                }
                OpenAdminMenu();
            }
            else
            {
                CheckCurrentAmount();
            }
        }
    }
}
