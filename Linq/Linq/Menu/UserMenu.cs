﻿using System;
using System.Linq;

namespace Linq.Menu
{
    public static class UserMenu
    {
        public static void OpenUserMenu(string userIn)
        {
            Console.WriteLine("Для отображения информации об аккаунте, нажмите 1.");
            Console.WriteLine("Для вывода данных о всех Ваших счетах, нажмите 2.");
            Console.WriteLine("Для вывода данных о всех Ваших счетах, включая историю, нажмите 3.");
            Console.WriteLine("Для выхода, нажмите 0.");
            string userInput = Console.ReadLine();

            if (userInput == "1")
            {
                UserInfo(userIn);
            }
            else if (userInput == "2")
            {
                AccountInfo(userIn);
            }
            else if (userInput == "3")
            {
                AccountHistoryInfo(userIn);
            }
            else if (userInput == "0")
            {
                AuthMenu.InputLogin();
            }
            else
            {
                Console.WriteLine("Введено неверное значение.");
                OpenUserMenu(userIn);
            }
        }
        public static void UserInfo(string userIn)
        {
            var uInfo = (from u in DemoData.users
                         where u.Login.ToLower() == userIn.ToLower()
                         select new { u.FIO, u.TelNumb, u.RegDate, u.Passp }).ElementAt(0);
            Console.WriteLine(uInfo.FIO.ToString() + " Телефон: " + uInfo.TelNumb
                + " Паспорт " + uInfo.Passp.ToString() + " Дата регистрации: " + uInfo.RegDate);
            OpenUserMenu(userIn);
        }
        public static void AccountInfo(string userIn)
        {
            var aInfo = DemoData.users.Join(DemoData.accounts,
                u => u.Id,
                a => a.UserId,
                (u, a) => new { u.Login, a.Id, a.OpenDate, a.CurrentAmount })
                .Where(x => x.Login.ToLower() == userIn.ToLower());
            if (aInfo.Count() > 0)
            {
                foreach (var acc in aInfo)
                {
                    Console.WriteLine($"Счет {acc.Id} Дата открытия: {acc.OpenDate.ToShortDateString()} Остаток: {acc.CurrentAmount}");
                }
            }
            else
            {
                Console.WriteLine("У Вас не открыто ни одного счета.");
            }
            OpenUserMenu(userIn);
        }
        public static void AccountHistoryInfo(string userIn)
        {
            var hInfo = from a in DemoData.accounts
                        join u in DemoData.users on a.UserId equals u.Id
                        join h in DemoData.histories on a.Id equals h.AccountId
                        where u.Login.ToLower() == userIn.ToLower()
                        select new { a.Id, h.OperationDate, h.OperationType, h.Amount };

            if (hInfo.Count() > 0)
            {
                foreach (var hist in hInfo)
                {
                    Console.WriteLine($"Счет {hist.Id} Дата операции: {hist.OperationDate.ToShortDateString()}"
                        + $" Тип операции: {hist.OperationType} Сумма операции: {hist.Amount}");
                }
            }
            else
            {
                Console.WriteLine("У Вас не открыто ни одного счета.");
            }
            OpenUserMenu(userIn);
        }
    }
}
