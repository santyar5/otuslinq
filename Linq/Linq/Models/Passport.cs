﻿namespace Linq.Models
{ 
    public class Passport
    {
        public string Ser { get; set; }
        public string Num { get; set; }
        public Passport(string serial, string number)
        {
            this.Ser = serial;
            this.Num = number;
        }
        public override string ToString()
        {
            return $"Серия {Ser} Номер {Num}";
        }
    }
}
